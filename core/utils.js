const fs = require('fs');

function createFolderIfNotExists(dir) {
    if(!fs.existsSync(dir)) {
        fs.mkdirSync(dir);
    }
}

module.exports = {
    createFolderIfNotExists
};
