const express   = require('express');
const cors      = require('cors');
const mongoose  = require('mongoose');
const fs        = require('fs');
const {
    createFolderIfNotExists
} = require('./core/utils');
const multer    = require('multer');
const unzipper  = require('unzipper');
const axios = require('axios');
const {distance, closest} = require('fastest-levenshtein');
// const mongo_express = require('mongo-express/lib/middleware');
// const mongo_express_config = require('./mongo_express/config.js');

const {
    EventEmitter,
    MultiPromise
} = require('./core/eventSystem');

mongoose.set('useNewUrlParser', true);
mongoose.set('useFindAndModify', false);
mongoose.set('useCreateIndex', true);

const multerStorage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, `${process.cwd()}/uploads/`)
    },
    filename: (req, file, cb) => {
        cb(null, file.fieldname + '-' + Date.now() + '.' + file.originalname.split('.')[file.originalname.split('.').length - 1].toLowerCase())
    }
});


const CustomizeProduct  = require('./schemas/customizeProduct');
const ShopifyStore      = require('./schemas/shopifyStore');
const Resource          = require('./schemas/resource');
const { response }      = require('express');

const app = express();
const router = express.Router();
const port = 3000;
// app.use(express.bodyParser({limit: '50mb'}));
app.use(cors({
    methods: ['GET','POST','DELETE','UPDATE','PUT','PATCH']
}));
router.use(cors({
    methods: ['GET','POST','DELETE','UPDATE','PUT','PATCH']
}));
router.use(express.json({limit: '50mb'}));
app.use(multer({ storage: multerStorage, dest: './uploads/' }).single('file'));
app.use('/design-upload-v2', router);

app.use('/design-upload-v2/view', express.static('view/dist'));

const mongoDB = 'mongodb://127.0.0.1/customize_management';
mongoose.connect(mongoDB, {useNewUrlParser: true, useUnifiedTopology: true});
const db = mongoose.connection;

router.get('/api/products', (req, res) => {
    const query = CustomizeProduct.find().select({
        sku: 1,
        createdAt: 1,
        iframeURL: 1,
    });
    query.exec((err, data) => {
        if(err) {
            return res.status(500).json({
                message: `${err.message}`
            });
        }
        res.json({
            data: data
        });
    });
});

router.get('/api/products/:productID', (req, res) => {
    const query = CustomizeProduct.findOne({ _id: req.params.productID });
    query.exec((err, data) => {
        res.json({
            data: data
        });
    })
});

router.post('/api/products', (req, res) => {
    const sku = req.body.sku;
    const iframeURL = req.body.iframeURL ?? '';
    const data = req.body.data ?? {};
    const subproducts = [];
    const shopifyProducts = [];

    const product = new CustomizeProduct({
        sku,
        iframeURL,
        data,
        subproducts,
        shopifyProducts
    });

    product.save((err, doc) => {
        createFolderIfNotExists('./data/products/' + sku);
        if(err) {
            return res.status(500).json({
                message: `${err.message}`
            });
        }
        res.json({
            data: doc
        });
    });
});

router.put('/api/products/:productID', async (req, res) => {
    const sku = req.body.sku;
    const iframeURL = req.body.iframeURL ?? '';
    const data = req.body.data ?? {};
    const subproducts = [];
    const shopifyProducts = [];

    const product = await CustomizeProduct.findOneAndUpdate({_id: req.params.productID}, {
        sku,
        iframeURL,
        data,
        subproducts,
        shopifyProducts
    }, { returnOriginal: false });

    product.save((err, doc) => {
        createFolderIfNotExists('./data/products/' + sku);
        if(err) {
            return res.status(500).json({
                message: `${err.message}`
            });
        }
        res.json({
            data: doc
        });
    });
});

router.delete('/api/products/:productID', (req, res) => {
    CustomizeProduct.deleteOne({ _id: req.params.productID }, err => {
        if(err) {
            return res.status(500).json({
                message: `${err.message}`
            });
        }
        res.json({
            message: "Product removed."
        });
    });
});

router.post('/api/products/search', async (req, res) => {
    const sku = req.body.sku;
    CustomizeProduct.find({}).exec((err, data) => {
        if(err) {
            return res.status(500).json({
                message: `${err.message}`
            });
        }
        const bestMatchSKU = closest(sku, data.map(product => product.sku));
        const bestMatchProduct = data.filter(product => product.sku == bestMatchSKU)[0];
        res.json({
            data: {
                bestMatch: bestMatchProduct
            }
        });
    });
});

router.post('/api/product_upload', (req, res) => {
    const file = req.file;
    if(file.mimetype != 'application/x-zip-compressed') {
        res.status(400).json({
            message: 'Uploaded file type is not zip'
        });
    }
    const stream = fs.createReadStream(file.path)
    stream.pipe(unzipper.Extract({ path: 'data/products/'+file.originalname.replace('.zip', '') }));
    stream.on('close', () => {
        setTimeout(() => {
            axios.post('https://dev.goodsmize.com/upload_aws_accelerate', {
                sku: file.originalname.replace('.zip', '')
            })
            .then(() => {
                res.json({
                    message: 'Uploaded.'
                });
            })
            .catch(() => {
                res.json({
                    message: 'Uploaded.'
                });
            });
        }, 5000);
    })
});

router.post('/api/data_json_sync/:sku', async (req, res) => {
    try {
        const dataJSON = fs.readFileSync('data/products/'+req.params.sku+'/data.json');
        await CustomizeProduct.findOneAndUpdate({sku: req.params.sku}, {
            data: JSON.parse(dataJSON)
        }, { returnOriginal: false });
        res.json({
            message: 'Done.'
        });
    }
    catch(e) {
        res.status(404).json({
            message: e.message
        });
    }
});

router.post('/api/data_json_backup/:sku', async (req, res) => {
    const product = CustomizeProduct.findOne({ sku: req.params.sku })
    .exec((err, data) => {
        if(err) {
            return res.status(500).json({
                message: err.message
            });
        }
        const datajson = data.data;
        fs.writeFileSync('data/products/'+req.params.sku+'/data.json', JSON.stringify(datajson));
        res.json({
            message: 'Done.'
        });
    });
});

router.post('/api/check_sku/:sku', (req, res) => {
    const query = CustomizeProduct.find({ sku: req.params.sku });
    query.exec((err, data) => {
        res.json({
            data: !!data.length
        });
    });
});

router.get('/api/shopify_stores', (req, res) => {
    const query = ShopifyStore.find();
    query.exec((err, data) => {
        if(err) {
            return res.status(500).json({
                message: `${err.message}`
            });
        }
        res.json({
            data: data
        });
    });
});

router.post('/api/shopify_stores', (req, res) => {
    const name          = req.body.name;
    const accessKey     = req.body.accessKey ?? '';
    const shopifyName   = req.body.shopifyName ?? '';
    const storeURL      = req.body.storeURL ?? '';

    const store = new ShopifyStore({
        name,
        accessKey,
        shopifyName,
        storeURL
    });

    store.save((err, doc) => {
        if(err) {
            return res.status(500).json({
                message: `${err.message}`
            });
        }
        res.json({
            data: doc
        });
    });
});

router.delete('/api/shopify_stores/:storeID', (req, res) => {
    ShopifyStore.deleteOne({ _id: req.params.storeID }, err => {
        if(err) {
            return res.status(500).json({
                message: `${err.message}`
            });
        }
        res.json({
            message: "Store removed."
        });
    });
});

router.post('/api/check_store_connection', async (req, res) => {
    const storeID = req.body.storeID;
    const store = await ShopifyStore.findOne({ _id: storeID });
    if(!store) return res.json({
        message: 'Error'
    });
    axios.get(`https://${store.accessKey}@${store.shopifyName}.myshopify.com/admin/shop.json`)
    .then(response => {
        res.json({
            message: 'Connected'
        });
    })
    .catch(err => {
        res.json({
            message: 'Error'
        });
    });
});

router.post('/api/store_search', async (req, res) => {
    const storeID = req.body.storeID;
    const store = await ShopifyStore.findOne({ _id: storeID });
    const searchString = req.body.searchString;
    axios.get(`https://${store.accessKey}@${store.shopifyName}.myshopify.com/search/suggest.json?q=${searchString}*&resources[type]=product&resources[fields]=title%2Cvariants.sku`)
    .then(response => {
        if(response.data.resources.results.products.length == 0) {
            axios.get(`https://${store.accessKey}@${store.shopifyName}.myshopify.com/products/${req.body.searchString}.json`)
            .then(r => {
                r.data.product.image = r.data.product.image.src;
                response.data.resources.results.products.push(r.data.product);
                res.json(response.data);
            })
            .catch(err => {
                res.json(response.data);
            })
        }
        else {
            res.json(response.data);
        }
    })
    .catch(err => {
        axios.get(`https://${store.accessKey}@${store.shopifyName}.myshopify.com/admin/products.json?handle=${req.body.searchString}`)
        .then(r => {
            try {
                r.data.products[0].image = r.data.products[0].image.src;
                response.data.resources.results.products.push(r.data.product);
            }
            catch(e) {}
            res.json({
                resources: {
                    results: {
                        products: r.data.products
                    }
                }
            });
        })
        .catch(err => {
            res.status(500).json({
                message: err.message
            });
        })
    })
});

router.post('/api/get_metafields', async (req, res) => {
    const storeID = req.body.storeID;
    const store = await ShopifyStore.findOne({ _id: storeID });
    const product = req.body.product;
    const metafieldURL = `https://${store.accessKey}@${store.shopifyName}.myshopify.com/admin/products/${product.id}/metafields.json`;
    axios.get(metafieldURL)
    .then(response => {
        res.json(response.data);
    })
    .catch(err => {
        res.status(500).json({
            message: err.message
        });
    });
});

router.post('/api/set_metafields', async (req, res) => {
    const storeID = req.body.storeID;
    const store = await ShopifyStore.findOne({ _id: storeID });
    const product = req.body.product;
    const metafields = req.body.metafields;
    const metafieldURL = `https://${store.accessKey}@${store.shopifyName}.myshopify.com/admin/products/${product.id}/metafields.json`;
    const multiPromise = new MultiPromise;
    for(const [metafieldKey, metafieldValue] of Object.entries(metafields)) {
        multiPromise.register(metafieldKey);
    }
    multiPromise.start().then(result => {
        res.json({
            data: result
        });
    });
    for(const [metafieldKey, metafieldValue] of Object.entries(metafields)) {
        axios.post(metafieldURL, {
            metafield: {
                namespace: 'TAT',
                key: metafieldKey,
                value: metafieldValue,
                value_type: 'string'
            }
        })
        .then(response => {
            multiPromise.fulfill(metafieldKey, response.data);
        })
        .catch(err => {
            multiPromise.fulfill(metafieldKey, err.message);
        });
    }
});

router.get('/products/:sku/data.json', (req, res) => {
    CustomizeProduct.findOne({ sku: req.params.sku })
    .exec((err, data) => {
        if(err) {
            return res.status(404).json({
                message: 'Product not found.'
            });
        }
        if(data.data) {
            res.json(data.data);
        }
        else {
            res.sendFile('./data/products/'+req.params.sku+'/data.json', { root: __dirname });
        }
    });
});

router.get('/products/:path([a-zA-Z0-9-_\\$\\/\\.\\% ]+)', (req, res) => {
    res.sendFile('./data/products/'+req.params.path, { root: __dirname }, function(err) {
        console.log(err);
    });
});

router.get('/api/resources', async (req, res) => {
    const query = Resource.find();
    query.exec((err, data) => {
        if(err) {
            return res.status(500).json({
                message: `${err.message}`
            });
        }
        res.json({
            data: data
        });
    });
});

router.post('/api/resources', async (req, res) => {
    const name = req.body.name;
    const category = req.body.category;
    const newResource = new Resource({
        name,
        category,
        path: '/resources/' + name
    });
    newResource.save((err, doc) => {
        createFolderIfNotExists('./data/resources/' + name);
        if(err) {
            return res.status(500).json({
                message: `${err.message}`
            });
        }
        res.json({
            data: doc
        });
    })
});

router.delete('/api/resources/:resourceID', async (req, res) => {
    try {
        const resourceID = req.params.resourceID;
        const resource = await Resource.findOne({ _id: resourceID });
        const resourcePath = './data/resources/' + resource.name;
        Resource.deleteOne({ _id: resourceID }, err => {
            if(err) return res.status(500).json({
                message: err.message
            });
            fs.rmdirSync(resourcePath, { recursive: true });
            res.json({
                message: 'Done.'
            });
        })
    }
    catch(err) {
        res.status(500).json({
            message: err.message
        });
    }
});

router.get('/api/resources/:resourceID/files', async (req, res) => {
    try {
        const resourceID = req.params.resourceID;
        const resource = await Resource.findOne({ _id: resourceID });
        const resourcePath = './data/resources/' + resource.name;
        res.json({
            data: fs.readdirSync(resourcePath)
        });
    }
    catch(err) {
        res.status(500).json({
            message: err.message
        });
    }
});

router.post('/api/resources/:resourceID/remove_file', async (req, res) => {
    try {
        const resourceID = req.params.resourceID;
        const resource = await Resource.findOne({ _id: resourceID });
        const resourcePath = './data/resources/' + resource.name;
        const fileName = req.body.fileName;
        fs.unlinkSync(resourcePath + '/' + fileName);
        res.json({
            message: 'Done.'
        });
    }
    catch(err) {
        res.status(500).json({
            message: err.message
        });
    }
});

router.post('/api/resource_upload', (req, res) => {
    const resourceName = req.body.name;
    const file = req.file;
    fs.copyFile(file.path, 'data/resources/'+resourceName+'/'+file.originalname, (err) => {
        if(err) {
            return res.status(500).json({
                message: `${err.message}`
            });
        }
        res.json({
            message: 'Uploaded.'
        });
    });
});

router.post('/api/check_resource', async (req, res) => {
    const name = req.body.name;
    Resource.find({ name })
    .exec((err, data) => {
        if(err) {
            return res.status(500).json({
                message: `${err.message}`
            });
        }
        res.json({
            data: data
        });
    });
});

router.get('/resources/:path([a-zA-Z0-9-_\\$\\/\\.\\% ]+)', (req, res) => {
    res.sendFile('./data/resources/'+req.params.path, { root: __dirname });
});

router.get('/products-backup', (req, res) => {
    const files = fs.readdirSync('./products-backup/download');
    res.json({
        data: {
            files,
            total: files.length,
        }
    })
});

router.get('/item-exports/:itemID', (req, res) => {
    const itemID = req.params.itemID;
    try {
        const data = fs.readFileSync(`./item-exports/${itemID}.json`).toString();
        res.json(JSON.parse(data));
    }
    catch(e) {
        res.json({});
    }
});

router.post('/item-exports', (req, res) => {
    const data = req.body.data;
    const filename = req.body.filename;
    fs.writeFileSync(`./item-exports/${filename}.json`, JSON.stringify(data));
    res.send('ok');
});

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`);
});

process.on('uncaughtException', function (err) {
    console.log("Uncaught exception detected!");
    console.error(err);
});
