const mongoose = require('mongoose');
const { Schema } = mongoose;

const resource = new Schema({
    name: {
        type: String,
        unique: true
    },
    category: {
        type: String,
        enum: [
            "font",
            "image",
            "other",
            "complex",
        ]
    },
    path    : String
});

module.exports = mongoose.model('Resource', resource);
