const mongoose = require('mongoose');
const { Schema } = mongoose;

const shopifyStore = new Schema({
    name        : {
        type    : String,
        unique  : true,
        required: true
    },
    accessKey   : String,
    shopifyName : String,
    storeURL    : String
});

module.exports = mongoose.model('ShopifyStore', shopifyStore);
