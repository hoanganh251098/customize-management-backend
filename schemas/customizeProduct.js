const mongoose = require('mongoose');
const { Schema } = mongoose;
const {
    createFolderIfNotExists
} = require('../core/utils');

const customizeProductSchema = new Schema({
    sku: {
        type    : String,
        unique  : true,
        required: true
    },
    iframeURL   : String,
    data        : Object,
    subproducts : [{
        name    : String,
        subproduct  : {
            type    : Schema.Types.ObjectId,
            ref     : 'CustomizeProduct'
        }
    }],
    shopifyProducts : [{
        shopifyStore: {
            type    : Schema.Types.ObjectId,
            ref     : 'ShopifyStore'
        },
        productID   : String
    }],
    createdAt: {
        type: Date,
        default: Date.now
    }
});

module.exports = mongoose.model('CustomizeProduct', customizeProductSchema);
